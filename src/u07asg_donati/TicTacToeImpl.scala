package u07asg_donati

import java.io.FileInputStream
import java.util.Optional

import alice._
import alice.tuprolog.{Struct, Theory}
import u07asg_donati.Scala2P._

import scala.collection.JavaConverters._
import scala.collection.mutable.Buffer

/**
  * Created by mirko on 4/10/17.
  */
class TicTacToeImpl(fileName: String) extends TicTacToe {

  implicit private def playerToString(player: Player): String = player match {
    case Player.PlayerX => "p1"
    case _ => "p2"
  }
  implicit private def stringToPlayer(s: String): Player = s match {
    case "p1" => Player.PlayerX
    case _ => Player.PlayerO
  }

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
  createBoard()

  override def createBoard() = {
    val goal = "retractall(board(_)),create_board(B),assert(board(B))"
    solveWithSuccess(engine,goal)
  }

  override def getBoard() = {
    val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map{
        case "null" => Optional.empty[Player]()
        case s => Optional.of[Player](s)
      }.to[Buffer].asJava
  }

  override def checkCompleted() = {
    val goal = "board(B),final(B,_)"
    solveWithSuccess(engine, goal)
  }

  override def checkVictory() = {
    val goal = "board(B),final(B,p1)"
    val goal2 = "board(B),final(B,p2)"
    if (solveWithSuccess(engine, goal)) Optional.of(Player.PlayerX)
    else if (solveWithSuccess(engine, goal2)) Optional.of(Player.PlayerO)
    else Optional.empty()
  }

  override def move(player: Player, i: Int, j: Int): Boolean = {
    println("Sono dentro il metodo move " + playerToString(player) + " " + i + " "+ j)
    val goal = s"board(B), next_board(B,${playerToString(player)},B2)"
    println("Ho realizzato il goal: " + goal)
    val nextboard = (for {
      term <- engine(goal).map(extractTerm(_, "B2"))
      elem = term.asInstanceOf[Struct].listIterator().asScala.toList(i + 5 * j)
      if (elem.toString == playerToString(player))
    } yield term).headOption
    println("Ho elaborato la nextboard: "+ nextboard)
    if (nextboard isEmpty) return false
    println("La nextboard non è vuota")
    val goal2 = s"retractall(board(_)), assert(board(${nextboard.get.toString}))"
    println("Ho realizzato il goal2: " + goal2)
    solveWithSuccess(engine,goal2)
  }

  override def toString =
    solveOneAndGetTerm(engine,"board(B)","B").toString


  override def checkLoseCompleted() = {
    val goal = "board(B),finallose(B,_)"
    solveWithSuccess(engine, goal)
  }

  override def checkLoss() = {
    val goal = "board(B),finallose(B,p1)"
    val goal2 = "board(B),finallose(B,p2)"
    if (solveWithSuccess(engine, goal)) Optional.of(Player.PlayerX)
    else if (solveWithSuccess(engine, goal2)) Optional.of(Player.PlayerO)
    else Optional.empty()
  }

}
