package u07asg_donati;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[5][5];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("TTT");
    private final JTextField countPlayerX;
    private final JTextField countPlayerO;
    private int counterPlayerX;
    private int counterPlayerO;
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int scoreToWin;

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt, int countPlayerX, int countPlayerO, int scoreToWin) throws Exception {
        this.ttt=ttt;
        this.scoreToWin = scoreToWin;
        this.counterPlayerX = countPlayerX;
        this.counterPlayerO = countPlayerO;
        this.countPlayerX = new JTextField(6);
        this.countPlayerX.setText("PlayerX: " + countPlayerX);
        this.countPlayerO = new JTextField(6);
        this.countPlayerO.setText("PlayerO: " + countPlayerO);
        System.out.println("Score to Win = " + scoreToWin);
        initPane();
    }
    
    private void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            changeTurn();
        }
        Optional<Player> loss = ttt.checkLoss();
        if (loss.isPresent()){
            exit.setText(loss.get()+" lose! " + "Play Again!");
            if((loss.get()+"").equals("PlayerX")){
                counterPlayerO++;
                countPlayerO.setText("PlayerO: "+counterPlayerO);
            }else{
                counterPlayerX++;
                countPlayerX.setText("PlayerX: "+counterPlayerX);
            }
            if(counterPlayerX == scoreToWin){
                exit.setEnabled(false);
                exit.setText("PlayerX Won the Game!");
            }else if(counterPlayerO == scoreToWin){
                exit.setEnabled(false);
                exit.setText("PlayerO Won the Game!");
            }
            finished=true;
            return;
        }
        if (ttt.checkLoseCompleted()){
            exit.setText("Even!");
            finished=true;
            return;
        }
        else {
            Optional<Player> victory = ttt.checkVictory();
            if (victory.isPresent()) {
                exit.setText(victory.get() + " won! " + "Play Again!");
                if((victory.get()+"").equals("PlayerX")){
                    counterPlayerX++;
                    countPlayerX.setText("PlayerX: "+counterPlayerX);
                }else{
                    counterPlayerO++;
                    countPlayerO.setText("PlayerO: "+counterPlayerO);
                }
                if(counterPlayerX == scoreToWin){
                    exit.setEnabled(false);
                    exit.setText("PlayerX Won the Game!");
                }else if(counterPlayerO == scoreToWin){
                    exit.setEnabled(false);
                    exit.setText("PlayerO Won the Game!");
                }
                finished = true;
                return;
            }
            if (ttt.checkCompleted()) {
                exit.setText("Even!");
                finished = true;
                return;
            }
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(5,5));
        for (int i=0;i<5;i++){
            for (int j=0;j<5;j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.setPreferredSize(new Dimension(70,70));
        s.add(countPlayerX);
        s.add(countPlayerO);
        s.add(exit);
        exit.addActionListener(e ->{
            if(exit.getText().equals("Exit")){
                System.exit(0);
            } else {
                try {
                    new TicTacToeApp(new TicTacToeImpl("src/u07asg_donati/ttt.pl"), counterPlayerX, counterPlayerO, this.scoreToWin);
                    frame.dispose();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(400,460);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            int min = 2;
            int max = 4;
            new TicTacToeApp(new TicTacToeImpl("src/u07asg_donati/ttt.pl"), 0, 0, (int) (min+Math.round(Math.random()*max)));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
