package u07asg_donati;

import java.util.List;
import java.util.Optional;

enum Player { PlayerX, PlayerO }

public interface
TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    boolean checkLoseCompleted();

    Optional<Player> checkLoss();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

}
